import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';

import ResultDetails from '../../src/js/components/Search/ResultDetails';
import store from '../../src/js/store';

// needed because this:
// https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

test('ResultDetails renders', () => {
  const component = renderer.create(
    <Provider store={store}>
      <ResultDetails
        wookiee={false}
        details={{
          population: 88,
          a: 47,
          b: 'star',
          c: 'wars'
        }}
        max={100}
        maxField='population'
        maxUnit='randoms'
      />
    </Provider>
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
