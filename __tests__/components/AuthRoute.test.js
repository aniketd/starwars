import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import createMemoryHistory from 'history/createMemoryHistory';

import AuthRoute from '../../src/js/components/Login/AuthRoute';
import store from '../../src/js/store';

const history = createMemoryHistory('/search');

const routes = [{
  path: '/search',
  component: () => (
    <AuthRoute
      path='/search'
      component={() => (<p>AuthSuccess</p>)}
    />
  )
}];

// needed because this:
// https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

test('Login renders', () => {
  global.localStorage = {
    _STARWARS: 'dummy',
    getItem: () => this._STARWARS
  };
  global.localStorage._STARWARS = undefined;
  const component = renderer.create(
    <Provider store={store}>
      <Router routes={routes} history={history} />
    </Provider>
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
  global.localStorage._STARWARS = 'Aniket Deshpande';
  expect(tree).toMatchSnapshot();
});
