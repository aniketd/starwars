import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import createMemoryHistory from 'history/createMemoryHistory';

import Login from '../../src/js/components/Login';
import store from '../../src/js/store';

const history = createMemoryHistory('/');

const routes = [{
  path: '/',
  component: () => <Login history={history} errors={[]} doLogin={() => (null)} />
}];

// needed because this:
// https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

test('Login renders', () => {
  const component = renderer.create(
    <Provider store={store}>
      <Router routes={routes} history={history} />
    </Provider>
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
