import React from 'react';
import renderer from 'react-test-renderer';
import { IntlProvider, addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en';
import nl from 'react-intl/locale-data/nl';
import ru from 'react-intl/locale-data/ru';
import { getCurrentLocale, getLocaleData } from 'grommet/utils/Locale';

import NoResults from '../../src/js/components/Search/NoResults';

const locale = getCurrentLocale();
addLocaleData([...en, ...nl, ...ru]);
let messages;
try {
  messages = require(`../../src/js/messages/${locale}`);
} catch (e) {
  messages = require('../../src/js/messages/en-US');
}
const localeData = getLocaleData(messages, locale);

// needed because this:
// https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

test('NoResults renders', () => {
  const component = renderer.create(
    <IntlProvider locale={localeData.locale} messages={localeData.messages}>
      <NoResults resource='planets' />
    </IntlProvider>
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
