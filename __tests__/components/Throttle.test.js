import React from 'react';
import renderer from 'react-test-renderer';

import Throttle from '../../src/js/components/Search/Throttle';

// needed because this:
// https://github.com/facebook/jest/issues/1353
jest.mock('react-dom');

test('Throttle renders', () => {
  const component = renderer.create(
    <Throttle enabled={true} />
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
