import reducer, { initialState } from '../../src/js/reducers/session';
import * as types from '../../src/js/actions';

describe('session reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should return a loaded session', () => {
    expect(reducer(undefined, {
      type: types.SESSION_LOAD,
      payload: 'Darth Vader',
      errors: []
    })).toEqual({
      name: 'Darth Vader',
      errors: []
    });
  });

  it('should return a logged in session', () => {
    expect(reducer(undefined, {
      type: types.SESSION_LOGIN,
      payload: 'Darth Vader',
      errors: ['You are a villian!']
    })).toEqual({
      name: 'Darth Vader',
      errors: ['You are a villian!']
    });
  });

  it('should return a logged out session', () => {
    expect(reducer({
      name: 'Darth Vader',
      errors: ['You are a villian!']
    }, {
      type: types.SESSION_LOGOUT
    })).toEqual(initialState);
  });
});
