import reducer, { initialState } from '../../src/js/reducers/search';
import * as types from '../../src/js/actions';

describe('search reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should return received planets', () => {
    expect(reducer(undefined, {
      type: types.SEARCH_RECEIVE_PLANETS,
      payload: [{
        population: 88,
        a: 'star',
        b: 'wars'
      }],
      next: 'Darth Vader'
    })).toEqual({
      ...initialState,
      planets: [{
        population: 88,
        a: 'star',
        b: 'wars'
      }],
      planetsMax: 88,
      planetsNext: 'Darth Vader'
    });
  });

  it('should return more received planets', () => {
    const seed = {
      planets: [{
        population: 88,
        a: 'star',
        b: 'wars'
      }],
      planetsMax: 88,
      planetsNext: 'Darth Vader'
    };
    const state = {
      ...initialState,
      ...seed
    };
    expect(reducer(state, {
      type: types.SEARCH_RECEIVE_MORE_PLANETS,
      payload: [{
        population: 99,
        a: 'master',
        b: 'yoda'
      }],
      next: 'Darth Vader Next'
    })).toEqual({
      ...initialState,
      planets: [...seed.planets, {
        population: 99,
        a: 'master',
        b: 'yoda'
      }],
      planetsMax: 99,
      planetsNext: 'Darth Vader Next'
    });
  });

  it('should return received planets wookiee', () => {
    expect(reducer(undefined, {
      type: types.SEARCH_RECEIVE_PLANETS_WOOKIEE,
      payload: [{
        population: 88,
        a: 'star',
        b: 'wars'
      }]
    })).toEqual({
      ...initialState,
      planetsWookiee: [{
        population: 88,
        a: 'star',
        b: 'wars'
      }]
    });
  });

  it('should return more received planets wookiee', () => {
    const seed = {
      planetsWookiee: [{
        population: 88,
        a: 'star',
        b: 'wars'
      }]
    };
    const state = {
      ...initialState,
      ...seed
    };
    expect(reducer(state, {
      type: types.SEARCH_RECEIVE_MORE_PLANETS_WOOKIEE,
      payload: [{
        population: 99,
        a: 'master',
        b: 'yoda'
      }]
    })).toEqual({
      ...initialState,
      planetsWookiee: [...seed.planetsWookiee, {
        population: 99,
        a: 'master',
        b: 'yoda'
      }]
    });
  });

  it('should return received species', () => {
    expect(reducer(undefined, {
      type: types.SEARCH_RECEIVE_SPECIES,
      payload: [{
        average_height: 88,
        a: 'star',
        b: 'wars'
      }],
      next: 'Darth Vader'
    })).toEqual({
      ...initialState,
      species: [{
        average_height: 88,
        a: 'star',
        b: 'wars'
      }],
      speciesMax: 88,
      speciesNext: 'Darth Vader'
    });
  });

  it('should return more received species', () => {
    const seed = {
      species: [{
        average_height: 88,
        a: 'star',
        b: 'wars'
      }],
      speciesMax: 88,
      speciesNext: 'Darth Vader'
    };
    const state = {
      ...initialState,
      ...seed
    };
    expect(reducer(state, {
      type: types.SEARCH_RECEIVE_MORE_SPECIES,
      payload: [{
        average_height: 99,
        a: 'master',
        b: 'yoda'
      }],
      next: 'Darth Vader Next'
    })).toEqual({
      ...initialState,
      species: [...seed.species, {
        average_height: 99,
        a: 'master',
        b: 'yoda'
      }],
      speciesMax: 99,
      speciesNext: 'Darth Vader Next'
    });
  });

  it('should return received species wookiee', () => {
    expect(reducer(undefined, {
      type: types.SEARCH_RECEIVE_SPECIES_WOOKIEE,
      payload: [{
        average_height: 88,
        a: 'star',
        b: 'wars'
      }]
    })).toEqual({
      ...initialState,
      speciesWookiee: [{
        average_height: 88,
        a: 'star',
        b: 'wars'
      }]
    });
  });

  it('should return more received species wookiee', () => {
    const seed = {
      speciesWookiee: [{
        average_height: 88,
        a: 'star',
        b: 'wars'
      }]
    };
    const state = {
      ...initialState,
      ...seed
    };
    expect(reducer(state, {
      type: types.SEARCH_RECEIVE_MORE_SPECIES_WOOKIEE,
      payload: [{
        average_height: 99,
        a: 'master',
        b: 'yoda'
      }]
    })).toEqual({
      ...initialState,
      speciesWookiee: [...seed.speciesWookiee, {
        average_height: 99,
        a: 'master',
        b: 'yoda'
      }]
    });
  });

  it('should return received people', () => {
    expect(reducer(undefined, {
      type: types.SEARCH_RECEIVE_PEOPLE,
      payload: [{
        mass: 88,
        a: 'star',
        b: 'wars'
      }],
      next: 'Darth Vader'
    })).toEqual({
      ...initialState,
      people: [{
        mass: 88,
        a: 'star',
        b: 'wars'
      }],
      peopleMax: 88,
      peopleNext: 'Darth Vader'
    });
  });

  it('should return more received people', () => {
    const seed = {
      people: [{
        mass: 88,
        a: 'star',
        b: 'wars'
      }],
      peopleMax: 88,
      peopleNext: 'Darth Vader'
    };
    const state = {
      ...initialState,
      ...seed
    };
    expect(reducer(state, {
      type: types.SEARCH_RECEIVE_MORE_PEOPLE,
      payload: [{
        mass: 99,
        a: 'master',
        b: 'yoda'
      }],
      next: 'Darth Vader Next'
    })).toEqual({
      ...initialState,
      people: [...seed.people, {
        mass: 99,
        a: 'master',
        b: 'yoda'
      }],
      peopleMax: 99,
      peopleNext: 'Darth Vader Next'
    });
  });

  it('should return received people wookiee', () => {
    expect(reducer(undefined, {
      type: types.SEARCH_RECEIVE_PEOPLE_WOOKIEE,
      payload: [{
        mass: 88,
        a: 'star',
        b: 'wars'
      }]
    })).toEqual({
      ...initialState,
      peopleWookiee: [{
        mass: 88,
        a: 'star',
        b: 'wars'
      }]
    });
  });

  it('should return more received people wookiee', () => {
    const seed = {
      peopleWookiee: [{
        mass: 88,
        a: 'star',
        b: 'wars'
      }]
    };
    const state = {
      ...initialState,
      ...seed
    };
    expect(reducer(state, {
      type: types.SEARCH_RECEIVE_MORE_PEOPLE_WOOKIEE,
      payload: [{
        mass: 99,
        a: 'master',
        b: 'yoda'
      }]
    })).toEqual({
      ...initialState,
      peopleWookiee: [...seed.peopleWookiee, {
        mass: 99,
        a: 'master',
        b: 'yoda'
      }]
    });
  });

  it('should return activated tab', () => {
    expect(reducer(undefined, {
      type: types.SEARCH_ACTIVATE_TAB,
      index: 9
    })).toEqual({
      ...initialState,
      activeTab: 9
    });
  });

  it('should reset reducer', () => {
    const state = {
      ...initialState,
      planets: [{}],
      species: [{}],
      people: [{}],
      wookiee: true
    };
    expect(reducer(state, {
      type: types.SEARCH_RESET
    })).toEqual({
      ...initialState,
      wookiee: true
    });
  });

  it('should toggle wookiee', () => {
    const state = {
      ...initialState,
      planets: [{}],
      species: [{}],
      people: [{}],
      wookiee: true
    };
    expect(reducer(state, {
      type: types.SEARCH_TOGGLE_WOOKIEE
    })).toEqual({
      ...state,
      wookiee: !state.wookiee
    });
  });
});
