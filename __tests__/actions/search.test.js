import * as actions from '../../src/js/actions/search';
import * as types from '../../src/js/actions';

describe('action creators', () => {
  it('should create an action to toggle wookiee', () => {
    const expectedAction = {
      type: types.SEARCH_TOGGLE_WOOKIEE
    };
    expect(actions.toggleWookiee()).toEqual(expectedAction);
  });

  it('should create an action to reset search', () => {
    const expectedAction = {
      type: types.SEARCH_RESET
    };
    expect(actions.resetSearch()).toEqual(expectedAction);
  });

  it('should create an action to activate a tab', () => {
    const activeTabIndex = 1;
    const expectedAction = {
      type: types.SEARCH_ACTIVATE_TAB,
      index: activeTabIndex
    };
    expect(actions.activateTab(activeTabIndex)).toEqual(expectedAction);
  });

  it('should create an action to receive planets', () => {
    const payload = {
      results: [{ luke: 'Skywalker' }],
      next: 'Darth Vader'
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_PLANETS,
      payload: payload.results,
      next: payload.next
    };
    expect(actions.receivePlanets(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive more planets', () => {
    const payload = {
      results: [{ luke: 'Skywalker' }],
      next: 'Darth Vader'
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_MORE_PLANETS,
      payload: payload.results,
      next: payload.next
    };
    expect(actions.receiveMorePlanets(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive planets in wookiee', () => {
    const payload = {
      rcwochuanaoc: [{ luke: 'Skywalker' }]
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_PLANETS_WOOKIEE,
      payload: payload.rcwochuanaoc
    };
    expect(actions.receivePlanetsWookiee(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive more planets in wookiee', () => {
    const payload = {
      rcwochuanaoc: [{ luke: 'Skywalker' }]
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_MORE_PLANETS_WOOKIEE,
      payload: payload.rcwochuanaoc
    };
    expect(actions.receiveMorePlanetsWookiee(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive species', () => {
    const payload = {
      results: [{ luke: 'Skywalker' }],
      next: 'Darth Vader'
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_SPECIES,
      payload: payload.results,
      next: payload.next
    };
    expect(actions.receiveSpecies(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive more species', () => {
    const payload = {
      results: [{ luke: 'Skywalker' }],
      next: 'Darth Vader'
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_MORE_SPECIES,
      payload: payload.results,
      next: payload.next
    };
    expect(actions.receiveMoreSpecies(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive species in wookiee', () => {
    const payload = {
      rcwochuanaoc: [{ luke: 'Skywalker' }]
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_SPECIES_WOOKIEE,
      payload: payload.rcwochuanaoc
    };
    expect(actions.receiveSpeciesWookiee(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive more species in wookiee', () => {
    const payload = {
      rcwochuanaoc: [{ luke: 'Skywalker' }]
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_MORE_SPECIES_WOOKIEE,
      payload: payload.rcwochuanaoc
    };
    expect(actions.receiveMoreSpeciesWookiee(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive people', () => {
    const payload = {
      results: [{ luke: 'Skywalker' }],
      next: 'Darth Vader'
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_PEOPLE,
      payload: payload.results,
      next: payload.next
    };
    expect(actions.receivePeople(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive more people', () => {
    const payload = {
      results: [{ luke: 'Skywalker' }],
      next: 'Darth Vader'
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_MORE_PEOPLE,
      payload: payload.results,
      next: payload.next
    };
    expect(actions.receiveMorePeople(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive people in wookiee', () => {
    const payload = {
      rcwochuanaoc: [{ luke: 'Skywalker' }]
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_PEOPLE_WOOKIEE,
      payload: payload.rcwochuanaoc
    };
    expect(actions.receivePeopleWookiee(payload)).toEqual(expectedAction);
  });

  it('should create an action to receive more people in wookiee', () => {
    const payload = {
      rcwochuanaoc: [{ luke: 'Skywalker' }]
    };
    const expectedAction = {
      type: types.SEARCH_RECEIVE_MORE_PEOPLE_WOOKIEE,
      payload: payload.rcwochuanaoc
    };
    expect(actions.receiveMorePeopleWookiee(payload)).toEqual(expectedAction);
  });
});
