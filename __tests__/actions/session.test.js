import * as actions from '../../src/js/actions/session';
import * as types from '../../src/js/actions';

describe('action creators', () => {
  it('should create an action to load the session', () => {
    const storage = 'Skywalker';
    const expectedAction = {
      type: types.SESSION_LOAD,
      payload: storage,
      errors: []
    };
    expect(actions.sessionLoad(storage)).toEqual(expectedAction);
  });

  it('should create an action to log into a session', () => {
    const payload = 'Skywalker';
    const errors = [];
    const expectedAction = {
      type: types.SESSION_LOGIN,
      payload,
      errors
    };
    expect(actions.sessionLogin(payload, errors)).toEqual(expectedAction);
  });

  it('should create an action to log out of a session', () => {
    const expectedAction = {
      type: types.SESSION_LOGOUT
    };
    expect(actions.sessionLogout()).toEqual(expectedAction);
  });
});
