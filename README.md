# Starwars

This is an assignment submission for assessment.

To run this application, execute the following commands:

  1. Install all dependencies

    ```
    $ yarn install
    ```

  2. Test and run linters:

    ```
    $ yarn test
    ```

  3. Start the front-end dev server:

    ```
    $ yarn dev
    ```

  4. Pack the app for production:

    ```
    $ yarn dist
    ```
