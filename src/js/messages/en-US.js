module.exports = {
  StarWars: 'Star Wars',
  LoginTitle: 'Encyclopedia Starwarsica',
  LoginPunchLine: 'May the force be with you',
  NoResults: 'What {resource} you search for may be beyond the known Universe!',
  Planets: 'Planets',
  Species: 'Species',
  People: 'People',
  planets: 'planets',
  species: 'species',
  people: 'people',
  Logout: 'Logout',
  NotFound: 'The adventure you are seeking does not exist.'
};
