module.exports = {
  StarWars: 'Ster Oorlogen',
  LoginTitle: 'Encyclopedie Steroorlogenica',
  LoginPunchLine: 'Mag de kracht bij jou zijn',
  NoResults: 'Welke {resource} je zoekt, kan verder dan het bekende heelal zijn',
  Planets: 'Planeten',
  Species: 'Soorten',
  People: 'Mensen',
  planets: 'planeten',
  species: 'soorten',
  people: 'mensen',
  Logout: 'Oitloggen',
  NotFound: 'Het avontuur dat u zoekt bestaat niet.'
};
