module.exports = {
  Username: 'имя пользователя',
  Title: 'заглавие',
  Password: 'пароль',
  'Log In': 'Авторизоваться',
  'Skip to': 'Пропустить',
  logout: 'выйти',
  search: 'поиск',
  'Tab Contents': 'табуляция содержание',
  Spinning: 'прядильный',

  StarWars: 'Звездные войны',
  LoginTitle: 'Энциклопедия Звездные войны',
  LoginPunchLine: 'Да прибудет с тобой сила',
  NoResults: 'Какой {resource} вы ищете, может быть за пределами известной Вселенной',
  Planets: 'планеты',
  Species: 'вид',
  People: 'люди',
  planets: 'планеты',
  species: 'вид',
  people: 'люди',
  Logout: 'Выйти',
  NotFound: 'Приключения, которого вы ищете, не существует.'
};
