// See https://github.com/acdlite/flux-standard-action

// Session
export const SESSION_LOAD = 'SESSION_LOAD';
export const SESSION_LOGIN = 'SESSION_LOGIN';
export const SESSION_LOGOUT = 'SESSION_LOGOUT';

// Search
export const SEARCH_RECEIVE_PLANETS = 'SEARCH_RECEIVE_PLANETS';
export const SEARCH_RECEIVE_MORE_PLANETS = 'SEARCH_RECEIVE_MORE_PLANETS';
export const SEARCH_RECEIVE_PLANETS_WOOKIEE = 'SEARCH_RECEIVE_PLANETS_WOOKIEE';
export const SEARCH_RECEIVE_MORE_PLANETS_WOOKIEE = 'SEARCH_RECEIVE_MORE_PLANETS_WOOKIEE';

export const SEARCH_RECEIVE_SPECIES = 'SEARCH_RECEIVE_SPECIES';
export const SEARCH_RECEIVE_MORE_SPECIES = 'SEARCH_RECEIVE_MORE_SPECIES';
export const SEARCH_RECEIVE_SPECIES_WOOKIEE = 'SEARCH_RECEIVE_SPECIES_WOOKIEE';
export const SEARCH_RECEIVE_MORE_SPECIES_WOOKIEE = 'SEARCH_RECEIVE_MORE_SPECIES_WOOKIEE';

export const SEARCH_RECEIVE_PEOPLE = 'SEARCH RECEIVE_PEOPLE';
export const SEARCH_RECEIVE_MORE_PEOPLE = 'SEARCH RECEIVE_MORE_PEOPLE';
export const SEARCH_RECEIVE_PEOPLE_WOOKIEE = 'SEARCH RECEIVE_PEOPLE_WOOKIEE';
export const SEARCH_RECEIVE_MORE_PEOPLE_WOOKIEE = 'SEARCH RECEIVE_MORE_PEOPLE_WOOKIEE';

export const SEARCH_ACTIVATE_TAB = 'SEARCH_ACTIVATE_TAB';
export const SEARCH_RESET = 'SEARCH_RESET';
export const SEARCH_TOGGLE_WOOKIEE = 'SEARCH_TOGGLE_WOOKIEE';
