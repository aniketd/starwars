import _ from 'lodash';
import {
  SESSION_LOAD, SESSION_LOGIN, SESSION_LOGOUT
} from '../actions';
import { login as getLogin } from '../api/session';

const localStorage = window.localStorage;

export const sessionLoad = payload => ({
  type: SESSION_LOAD,
  payload,
  errors: []
});

export const sessionLogin = (payload, errors) => ({
  type: SESSION_LOGIN,
  payload,
  errors
});

export const sessionLogout = () => ({
  type: SESSION_LOGOUT
});

export const initialize = () => (
  (dispatch) => {
    const { _STARWARS } = localStorage;
    if (!_.isNull(_STARWARS)) {
      dispatch(sessionLoad(_STARWARS));
    } else {
      window.location.href = '/login';
    }
  }
);

const verify = (dispatch, person, username, password, succCallback) => {
  if (person.name === username) {
    if (person.birth_year === password) {
      try {
        localStorage._STARWARS = person.name;
        dispatch(sessionLogin(person.name, []));
        succCallback();
      } catch (e) {
        console.error(e);
        alert(
          'Unable to preserve session, probably due to being in private ' +
            'browsing mode.'
        );
        dispatch(sessionLogin(
          null,
          ['Cannot login in Private Browsing Mode']
        ));
      }
    } else {
      dispatch(sessionLogin(
        null,
        [`${username}, remember, the force was strong when you were born.`]
      ));
    }
  } else {
    dispatch(sessionLogin(null, [`Yoda: Lazy, be not... ${person.name}`]));
  }
};

const handleIncompleteUsername = (dispatch, persons) =>
  dispatch(sessionLogin(
    null,
    [
      'Invalid username!',
      'Perhaps you meant one of the following?',
      ..._.map(_.take(persons, 5), p => `- ${p.name}`)
    ]
  ));

export const login = (username, password, succCallback) => (
  dispatch => (
    getLogin(username)
      .then((payload) => {
        switch (payload.count) {
          case 0:
            return dispatch(sessionLogin(null, ['Invalid username!']));
          case 1:
            return verify(
              dispatch,
              payload.results[0],
              username,
              password,
              succCallback
            );
          default:
            return handleIncompleteUsername(
              dispatch,
              payload.results
            );
        }
      })
  )
);

export const logout = () => (dispatch) => {
  dispatch(sessionLogout());
  try {
    localStorage.removeItem('_STARWARS');
  } catch (e) {
    // ignore
  }
  window.location.href = '/login'; // reload fully
};
