// ------------------------ in wookiee
// 'rcwochuanaoc' : results

import {
  SEARCH_RECEIVE_PLANETS,
  SEARCH_RECEIVE_MORE_PLANETS,
  SEARCH_RECEIVE_PLANETS_WOOKIEE,
  SEARCH_RECEIVE_MORE_PLANETS_WOOKIEE,
  SEARCH_RECEIVE_SPECIES,
  SEARCH_RECEIVE_MORE_SPECIES,
  SEARCH_RECEIVE_SPECIES_WOOKIEE,
  SEARCH_RECEIVE_MORE_SPECIES_WOOKIEE,
  SEARCH_RECEIVE_PEOPLE,
  SEARCH_RECEIVE_MORE_PEOPLE,
  SEARCH_RECEIVE_PEOPLE_WOOKIEE,
  SEARCH_RECEIVE_MORE_PEOPLE_WOOKIEE,
  SEARCH_ACTIVATE_TAB,
  SEARCH_RESET,
  SEARCH_TOGGLE_WOOKIEE
} from '../actions';
import { fetchResource, fetchMore } from '../api/search';

export const toggleWookiee = () => ({
  type: SEARCH_TOGGLE_WOOKIEE
});

export const resetSearch = () => ({
  type: SEARCH_RESET
});

export const activateTab = index => ({
  type: SEARCH_ACTIVATE_TAB,
  index
});

export const receivePlanets = payload => ({
  type: SEARCH_RECEIVE_PLANETS,
  payload: payload.results,
  next: payload.next
});

export const receiveMorePlanets = payload => ({
  type: SEARCH_RECEIVE_MORE_PLANETS,
  payload: payload.results,
  next: payload.next
});

export const receivePlanetsWookiee = payload => ({
  type: SEARCH_RECEIVE_PLANETS_WOOKIEE,
  payload: payload.rcwochuanaoc // see BOF
});

export const receiveMorePlanetsWookiee = payload => ({
  type: SEARCH_RECEIVE_MORE_PLANETS_WOOKIEE,
  payload: payload.rcwochuanaoc // see BOF
});

export const receiveSpecies = payload => ({
  type: SEARCH_RECEIVE_SPECIES,
  payload: payload.results,
  next: payload.next
});

export const receiveMoreSpecies = payload => ({
  type: SEARCH_RECEIVE_MORE_SPECIES,
  payload: payload.results,
  next: payload.next
});

export const receiveSpeciesWookiee = payload => ({
  type: SEARCH_RECEIVE_SPECIES_WOOKIEE,
  payload: payload.rcwochuanaoc // see BOF
});

export const receiveMoreSpeciesWookiee = payload => ({
  type: SEARCH_RECEIVE_MORE_SPECIES_WOOKIEE,
  payload: payload.rcwochuanaoc // see BOF
});

export const receivePeople = payload => ({
  type: SEARCH_RECEIVE_PEOPLE,
  payload: payload.results,
  next: payload.next
});

export const receiveMorePeople = payload => ({
  type: SEARCH_RECEIVE_MORE_PEOPLE,
  payload: payload.results,
  next: payload.next
});

export const receivePeopleWookiee = payload => ({
  type: SEARCH_RECEIVE_PEOPLE_WOOKIEE,
  payload: payload.rcwochuanaoc // see BOF
});

export const receiveMorePeopleWookiee = payload => ({
  type: SEARCH_RECEIVE_MORE_PEOPLE_WOOKIEE,
  payload: payload.rcwochuanaoc // see BOF
});

export const fetchPlanets = (input, wookiee) => (
  (dispatch) => {
    fetchResource('planets', input, wookiee)
      .then((payload) => {
        if (wookiee === true) {
          dispatch(receivePlanetsWookiee(payload));
        } else {
          dispatch(receivePlanets(payload));
        }
      });
  }
);

export const fetchMorePlanets = (url, wookiee) => (
  (dispatch) => {
    fetchMore(url, wookiee)
      .then((payload) => {
        if (wookiee === true) {
          dispatch(receiveMorePlanetsWookiee(payload));
        } else {
          dispatch(receiveMorePlanets(payload));
        }
      });
  }
);

export const fetchSpecies = (input, wookiee) => (
  (dispatch) => {
    fetchResource('species', input, wookiee)
      .then((payload) => {
        if (wookiee === true) {
          dispatch(receiveSpeciesWookiee(payload));
        } else {
          dispatch(receiveSpecies(payload));
        }
      });
  }
);

export const fetchMoreSpecies = (url, wookiee) => (
  (dispatch) => {
    fetchMore(url, wookiee)
      .then((payload) => {
        if (wookiee === true) {
          dispatch(receiveMoreSpeciesWookiee(payload));
        } else {
          dispatch(receiveMoreSpecies(payload));
        }
      });
  }
);

export const fetchPeople = (input, wookiee) => (
  (dispatch) => {
    fetchResource('people', input, wookiee)
      .then((payload) => {
        if (wookiee === true) {
          dispatch(receivePeopleWookiee(payload));
        } else {
          dispatch(receivePeople(payload));
        }
      });
  }
);

export const fetchMorePeople = (url, wookiee) => (
  (dispatch) => {
    fetchMore(url, wookiee)
      .then((payload) => {
        if (wookiee === true) {
          dispatch(receiveMorePeopleWookiee(payload));
        } else {
          dispatch(receiveMorePeople(payload));
        }
      });
  }
);
