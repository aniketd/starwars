import 'whatwg-fetch';

import {
  headers, buildQuery, processStatus
} from 'grommet/utils/Rest';

import { BASE_URL, parseJSON } from './utils';

export const login = (username) => {
  const query = buildQuery({ search: username });
  const options = {
    method: 'GET',
    headers
  };
  return fetch(`${BASE_URL}/people/${query}`, options)
    .then(processStatus)
    .then(parseJSON);
};

export default { login };
