// ----------------- in wookiee
// 'whhuanan'     : null
// 'rcwochuanaoc' : results


import 'whatwg-fetch';
import _ from 'lodash';

import {
  headers, buildParams, buildQuery, processStatus
} from 'grommet/utils/Rest';

import { BASE_URL } from './utils';

const parseNumber = (str) => {
  if (typeof str !== 'string') {
    return str;
  }
  const num = parseInt(_.replace(str, ',', ''), 10);
  if (_.isNaN(num)) {
    return str;
  }
  return num;
};

export const fetchResource = (resource, input, wookiee) => {
  let queryMap = { search: input };
  if (wookiee === true) {
    queryMap = {
      ...queryMap,
      format: 'wookiee'
    };
  }
  const query = buildQuery(queryMap);
  const options = {
    method: 'GET',
    headers
  };
  return fetch(`${BASE_URL}/${resource}/${query}`, options)
    .then(processStatus)
    .then(response => response.text())
    .then((responseText) => {
      if (wookiee === true) {
        const cleanedText = _.join(_.split(responseText, 'whhuanan'), 'null'); // see BOF
        const json = JSON.parse(cleanedText);
        return {
          ...json,
          rcwochuanaoc: _.map(
            json.rcwochuanaoc, // see BOF
            r => _.mapValues(r, parseNumber)
          )
        };
      }
      const json = JSON.parse(responseText);
      return {
        ...json,
        results: _.map(
          json.results,
          r => _.mapValues(r, parseNumber)
        )
      };
    });
};

export const fetchMore = (url, wookiee) => {
  let _url = url;
  if (wookiee === true) {
    _url = `${_url}&${buildParams({ format: 'wookiee' })}`;
  }
  const options = {
    method: 'GET',
    headers
  };
  return fetch(_url, options)
    .then(processStatus)
    .then(response => response.text())
    .then((responseText) => {
      if (wookiee === true) {
        const cleanedText = _.join(_.split(responseText, 'whhuanan'), 'null'); // see BOF
        const json = JSON.parse(cleanedText);
        return {
          ...json,
          rcwochuanaoc: _.map(
            json.rcwochuanaoc, // see BOF
            r => _.mapValues(r, parseNumber)
          )
        };
      }
      const json = JSON.parse(responseText);
      return {
        ...json,
        results: _.map(
          json.results,
          r => _.mapValues(r, parseNumber)
        )
      };
    });
};
