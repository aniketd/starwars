export const BASE_URL = 'http://swapi.co/api';

export const parseJSON = (response) => {
  if (response.ok) {
    return response.json();
  }
  return Promise.reject(response);
};
