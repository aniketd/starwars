import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage, IntlMixin, injectIntl } from 'react-intl';

import Headline from 'grommet/components/Headline';
import { getMessage } from 'grommet/utils/Intl';

class NoResults extends Component {

  constructor(props) {
    super(props);
    Object.assign(this, IntlMixin);
  }

  render() {
    const { resource, intl } = this.props;
    return (
      <Headline
        align='center'
        size='small'
        margin='small'
      >
        <FormattedMessage
          id='NoResults'
          defaultMessage={getMessage(intl, 'NoResults')}
          values={{ resource: <b>{resource}</b> }}
        />
      </Headline>
    );
  }
}

NoResults.propTypes = {
  resource: PropTypes.string.isRequired,
  intl: PropTypes.object.isRequired
};

export default injectIntl(NoResults);
