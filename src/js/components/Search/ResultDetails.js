/* -------------------------- in wookiee
 * 'oarcworaaowowa' : created
 * 'wowaahaowowa'   : edited
 * */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Box from 'grommet/components/Box';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import Meter from 'grommet/components/Meter';
import Value from 'grommet/components/Value';
import Label from 'grommet/components/Label';

class ResultDetails extends Component {

  static separateNumsNonNums(data, maxField) {
    const nums = _.pickBy(data, _.isNumber);
    let majorValue;
    let majorLabel;
    if (_.has(nums, maxField) === true) {
      majorValue = nums[maxField];
      majorLabel = majorValue;
    } else {
      majorValue = 0;
      majorLabel = 'unknown';
    }
    const otherNums = _.omit(nums, maxField);
    const nonNums = _.pickBy(data, v => !_.isNumber(v));
    return {
      majorValue,
      majorLabel,
      otherNums,
      nonNums
    };
  }

  constructor(props) {
    super(props);
    this.getDetails = this.getDetails.bind(this);
  }

  getDetails() {
    const { wookiee, details } = this.props;
    const filteredDetails = wookiee ?
            _.omit(details, ['oarcworaaowowa', 'wowaahaowowa']) :
            _.omit(details, ['created', 'edited']);
            // remove uninteresting, common fields
    const presentableDetails = _.pickBy(
      filteredDetails,
      val => _.isString(val) || _.isNumber(val)
    );
    return presentableDetails;
  }

  render() {
    const { max, maxField, maxUnit } = this.props;
    const details = this.getDetails();
    const {
      majorValue, majorLabel, otherNums, nonNums
    } = ResultDetails.separateNumsNonNums(details, maxField);
    const scaledValue = (majorValue * 100) / max; // scale to %
    return (
      <List>
        <ListItem
          key={`${maxField}-${majorValue}-${majorLabel}`}
          justify='between'
          separator='horizontal'
        >
          <Box>
            <Value
              value={majorValue === 0 ? majorLabel : majorValue}
              units={maxUnit}
              align='end'
              size='xsmall'
            />
            <Meter
              value={scaledValue}
              max={100}
              type='bar'
              vertical={false}
              size='medium'
            />
            <Box
              direction='row'
              justify='between'
              pad={{ between: 'small' }}
              responsive={false}
            >
              <Label size='small'>
                0 {maxUnit}
              </Label>
              <Label size='small'>
                {max} {maxUnit}
              </Label>
            </Box>
          </Box>
        </ListItem>
        {
          _.map(otherNums, (val, key) => (
            <ListItem
              key={`${key}-{val}`}
              justify='between'
              separator='horizontal'
            >
              <span>
                {key}
              </span>
              <span className='secondary'>
                {val}
              </span>
            </ListItem>
          ))
        }
        {
          _.map(nonNums, (val, key) => (
            <ListItem
              key={`${key}-{val}`}
              justify='between'
              separator='horizontal'
            >
              <span>
                {key}
              </span>
              <span className='secondary'>
                {val}
              </span>
            </ListItem>
          ))
        }
      </List>
    );
  }
}

ResultDetails.propTypes = {
  wookiee: PropTypes.bool.isRequired,
  details: PropTypes.object.isRequired,
  max: PropTypes.number.isRequired,
  maxField: PropTypes.string.isRequired,
  maxUnit: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  wookiee: state.search.wookiee
});

export default connect(mapStateToProps)(ResultDetails);
