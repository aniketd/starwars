/* ----------------- in wookiee
 * 'whwokao'  : next
 * 'whrascwo' : name
 * */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import moment from 'moment';

import SearchBar from 'grommet/components/Search';
import Box from 'grommet/components/Box';

import Throttle from './Throttle';
import Results from './Results';
import {
  fetchPlanets,
  fetchSpecies,
  fetchPeople,
  resetSearch
} from '../../actions/search';

class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {
      requestLog: [],
      disabled: false
    };
    this.handleInput = this.handleInput.bind(this);
    this._shouldThrottle = this._shouldThrottle.bind(this);
  }

  handleInput(event) {
    event.preventDefault();
    const input = event.target.value;
    const {
      doResetSearch,
      doFetchPlanets,
      doFetchSpecies,
      doFetchPeople
    } = this.props;
    if (input.length === 0) {
      return doResetSearch();
    }
    if (this._shouldThrottle() === false) {
      doFetchPlanets(input, false);
      doFetchSpecies(input, false);
      doFetchPeople(input, false);
      doFetchPlanets(input, true);
      doFetchSpecies(input, true);
      doFetchPeople(input, true);
      return null;
    }
    return this.setState({ disabled: true });
  }

  _shouldThrottle() {
    if (this.props.hero === 'Luke Skywalker') return false;
    const oldestRequestTime = _.min(this.state.requestLog);
    const now = moment.now();
    if ((now - oldestRequestTime) < (60 * 1000) &&
        this.state.requestLog.length > 15) {
      return true;
    }
    this.setState({
      requestLog: _.concat(this.state.requestLog, now)
    });
    setTimeout(() => {
      this.setState({
        requestLog: _.without(this.state.requestLog, now),
        disabled: false
      });
    }, (60 * 1000));
    return false;
  }

  render() {
    return (
      <Box basis='full' full='horizontal'>
        <SearchBar
          fill={true}
          inline={true}
          onDOMChange={this.handleInput}
          placeHolder='Search planets, species or people...'
        />
        <Throttle enabled={this.state.disabled} />
        <Results />
      </Box>
    );
  }
}

Search.propTypes = {
  hero: PropTypes.string,
  doResetSearch: PropTypes.func.isRequired,
  doFetchPlanets: PropTypes.func.isRequired,
  doFetchSpecies: PropTypes.func.isRequired,
  doFetchPeople: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  hero: state.session.name
});

const mapDispatchToProps = dispatch => ({
  doResetSearch: () => dispatch(resetSearch()),
  doFetchPlanets: (input, wookiee) => dispatch(fetchPlanets(input, wookiee)),
  doFetchSpecies: (input, wookiee) => dispatch(fetchSpecies(input, wookiee)),
  doFetchPeople: (input, wookiee) => dispatch(fetchPeople(input, wookiee))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
