/* ----------------- in wookiee
 * 'whrascwo' : name
 * */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';

import Tiles from 'grommet/components/Tiles';
import Tile from 'grommet/components/Tile';
import Card from 'grommet/components/Card';

import NoResults from './NoResults';
import ResultDetails from './ResultDetails';

class ResultsTab extends Component {

  constructor(props) {
    super(props);
    this.fetchMore = this.fetchMore.bind(this);
  }

  fetchMore() {
    const { nextUrl } = this.props;
    if (!_.isNull(nextUrl) && !_.isUndefined(nextUrl)) {
      this.props.fetcher();
    }
  }

  render() {
    const { data, resource, wookiee, max, maxField, maxUnit } = this.props;
    const nameString = wookiee === true ? 'whrascwo' : 'name'; // see BOF
    return (
      <Tiles
        justify='center'
        wrap={true}
        fill={true}
        flush={true}
        onMore={_.isEmpty(data) ? null : this.fetchMore}
        size={'large'}
      >
        {
          _.isEmpty(data) ?
            <NoResults resource={resource} /> :
            _.map(data, d => (
              <Tile key={d[nameString]}>
                <Card
                  heading={d[nameString]}
                  textSize='small'
                  description={
                    <ResultDetails
                      details={d}
                      max={max}
                      maxField={maxField}
                      maxUnit={maxUnit}
                    />
                  }
                />
              </Tile>
            ))
        }
      </Tiles>
    );
  }
}

ResultsTab.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  max: PropTypes.number.isRequired,
  maxField: PropTypes.string.isRequired,
  maxUnit: PropTypes.string.isRequired,
  resource: PropTypes.string.isRequired,
  nextUrl: PropTypes.string,
  wookiee: PropTypes.bool.isRequired,
  fetcher: PropTypes.func.isRequired
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  fetcher: () => {
    const func = ownProps.moreFetcher;
    const url = ownProps.nextUrl;
    dispatch(func(url, false)); // we need results for both formats
    dispatch(func(url, true));
  }
});

export default connect(null, mapDispatchToProps)(ResultsTab);
