import React from 'react';
import PropTypes from 'prop-types';

import Pulse from 'grommet/components/icons/Pulse';
import Status from 'grommet/components/icons/Status';

const Throttle = ({ enabled }) => (
  enabled ?
    <Pulse icon={<Status value='disabled' />} /> :
    null
);

Throttle.propTypes = {
  enabled: PropTypes.bool.isRequired
};

export default Throttle;
