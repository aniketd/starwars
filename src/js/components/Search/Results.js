import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Tabs from 'grommet/components/Tabs';
import Tab from 'grommet/components/Tab';

import ResultsTab from './ResultsTab';
import {
  activateTab,
  fetchMorePlanets,
  fetchMoreSpecies,
  fetchMorePeople,
} from '../../actions/search';

const Results = ({
  planets, species, people, tabActivator,
  planetsNext, speciesNext, peopleNext,
  planetsMax, speciesMax, peopleMax,
  planetsWookiee, speciesWookiee, peopleWookiee, wookiee
}) => (
  <Tabs
    justify='center'
    onActive={tabActivator}
  >
    <Tab title='Planets'>
      <ResultsTab
        data={wookiee === true ? planetsWookiee : planets}
        max={planetsMax}
        maxField={wookiee === true ? 'akooakhuanraaoahoowh' : 'population'}
        maxUnit='people'
        resource='planets'
        moreFetcher={fetchMorePlanets}
        nextUrl={planetsNext}
        wookiee={wookiee}
        index={0}
      />
    </Tab>
    <Tab title='Species'>
      <ResultsTab
        data={wookiee === true ? speciesWookiee : species}
        max={speciesMax}
        maxField={wookiee === true ? 'rahoworcrarrwo_acwoahrracao' : 'average_height'}
        maxUnit='cms'
        resource='species'
        moreFetcher={fetchMoreSpecies}
        nextUrl={speciesNext}
        wookiee={wookiee}
        index={1}
      />
    </Tab>
    <Tab title='People'>
      <ResultsTab
        data={wookiee === true ? peopleWookiee : people}
        max={peopleMax}
        maxField={wookiee === true ? 'scracc' : 'mass'}
        maxUnit='kgs'
        resource='people'
        moreFetcher={fetchMorePeople}
        nextUrl={peopleNext}
        wookiee={wookiee}
        index={2}
      />
    </Tab>
  </Tabs>
);

Results.propTypes = {
  planets: PropTypes.arrayOf(PropTypes.object).isRequired,
  planetsWookiee: PropTypes.arrayOf(PropTypes.object).isRequired,
  planetsMax: PropTypes.number.isRequired,
  planetsNext: PropTypes.string,
  species: PropTypes.arrayOf(PropTypes.object).isRequired,
  speciesWookiee: PropTypes.arrayOf(PropTypes.object).isRequired,
  speciesMax: PropTypes.number.isRequired,
  speciesNext: PropTypes.string,
  people: PropTypes.arrayOf(PropTypes.object).isRequired,
  peopleWookiee: PropTypes.arrayOf(PropTypes.object).isRequired,
  peopleMax: PropTypes.number.isRequired,
  peopleNext: PropTypes.string,
  wookiee: PropTypes.bool.isRequired,
  tabActivator: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  planets: state.search.planets,
  planetsWookiee: state.search.planetsWookiee,
  planetsMax: state.search.planetsMax,
  planetsNext: state.search.planetsNext,
  species: state.search.species,
  speciesWookiee: state.search.speciesWookiee,
  speciesMax: state.search.speciesMax,
  speciesNext: state.search.speciesNext,
  people: state.search.people,
  peopleWookiee: state.search.peopleWookiee,
  peopleMax: state.search.peopleMax,
  peopleNext: state.search.peopleNext,
  wookiee: state.search.wookiee
});

const mapDispatchToProps = dispatch => ({
  tabActivator: index => dispatch(activateTab(index))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Results);
