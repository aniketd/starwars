import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { injectIntl } from 'react-intl';

import LoginForm from 'grommet/components/LoginForm';
import { getMessage } from 'grommet/utils/Intl';

import { login } from '../../actions/session';

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      errors: []
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit({ username, password }) {
    this.props.doLogin(
      username,
      password,
      () => this.props.history.push('/search')
    );
  }

  render() {
    const { intl } = this.props;
    return (
      <LoginForm
        align='center'
        defaultValues={{ username: 'Luke Skywalker', rememberMe: false }}
        title={getMessage(intl, 'LoginTitle')}
        usernameType='text'
        secondaryText={getMessage(intl, 'LoginPunchLine')}
        onSubmit={this.handleSubmit}
        errors={this.props.errors}
      />
    );
  }
}

Login.propTypes = {
  doLogin: PropTypes.func.isRequired,
  errors: PropTypes.arrayOf(PropTypes.string).isRequired,
  history: PropTypes.object.isRequired,
  intl: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.session.errors
});

const mapDispatchToProps = dispatch => ({
  doLogin: (username, password, callback) =>
    dispatch(login(username, password, callback))
});

export default injectIntl(withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Login)));
