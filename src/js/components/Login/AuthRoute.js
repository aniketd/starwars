import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import _ from 'lodash';

const localStorage = window.localStorage;

export const Authenticated = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={({ location, ...props }) => (
      _.isUndefined(localStorage._STARWARS) ?
        <Redirect
          to={{ pathname: '/login', state: { from: location } }}
        /> :
        <Component location={location} {...props} />
    )}
  />
);

Authenticated.propTypes = {
  component: PropTypes.func.isRequired,
};

export default Authenticated;
