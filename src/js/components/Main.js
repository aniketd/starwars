import React from 'react';
import {
  BrowserRouter as Router, Route, Switch
} from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';

import App from 'grommet/components/App';
import Article from 'grommet/components/Article';
import Footer from 'grommet/components/Footer';
import Paragraph from 'grommet/components/Paragraph';
import Anchor from 'grommet/components/Anchor';

import Login from '../screens/Login';
import Search from '../screens/Search';
import NotFound from '../screens/NotFound';
import { Authenticated } from './Login/AuthRoute';

const history = createBrowserHistory();

const Main = () => (
  <App>
    <Article>
      <Router history={history}>
        <Switch>
          <Route exact={true} path='/' component={Login} />
          <Route path='/login' component={Login} />
          <Authenticated path='/search' component={Search} />
          <Route path='/*' component={NotFound} />
        </Switch>
      </Router>
      <Footer
        full='horizontal'
        justify='center'
        separator='top'
      >
        <Paragraph>© <Anchor
          id='xl-anchor'
          href='https://xebialabs.com'
          label='XebiaLabs'
          target='_blank noopener noreferrer'
        />
        </Paragraph>
      </Footer>
    </Article>
  </App>
);

export default Main;
