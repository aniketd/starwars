import _ from 'lodash';

import {
  SEARCH_RECEIVE_PLANETS,
  SEARCH_RECEIVE_MORE_PLANETS,
  SEARCH_RECEIVE_PLANETS_WOOKIEE,
  SEARCH_RECEIVE_MORE_PLANETS_WOOKIEE,
  SEARCH_RECEIVE_SPECIES,
  SEARCH_RECEIVE_MORE_SPECIES,
  SEARCH_RECEIVE_SPECIES_WOOKIEE,
  SEARCH_RECEIVE_MORE_SPECIES_WOOKIEE,
  SEARCH_RECEIVE_PEOPLE,
  SEARCH_RECEIVE_MORE_PEOPLE,
  SEARCH_RECEIVE_PEOPLE_WOOKIEE,
  SEARCH_RECEIVE_MORE_PEOPLE_WOOKIEE,
  SEARCH_ACTIVATE_TAB,
  SEARCH_RESET,
  SEARCH_TOGGLE_WOOKIEE
} from '../actions';
import { createReducer } from './utils';

export const initialState = {
  planets: [],
  planetsWookiee: [],
  planetsNext: null,
  planetsMax: 0,
  species: [],
  speciesWookiee: [],
  speciesNext: null,
  speciesMax: 0,
  people: [],
  peopleWookiee: [],
  peopleNext: null,
  peopleMax: 0,
  activeTab: 0,
  wookiee: false
};

const handlers = {
  [SEARCH_RECEIVE_PLANETS]: (state, action) => {
    const maxPlanet = _.maxBy(action.payload, p => p.population);
    return {
      planets: action.payload,
      planetsMax: maxPlanet.population,
      planetsNext: action.next
    };
  },
  [SEARCH_RECEIVE_MORE_PLANETS]: (state, action) => {
    const planets = _.concat(state.planets, action.payload);
    const maxPlanet = _.maxBy(planets, p => p.population);
    return {
      planets,
      planetsMax: maxPlanet.population,
      planetsNext: action.next
    };
  },
  [SEARCH_RECEIVE_PLANETS_WOOKIEE]: (state, action) => ({
    planetsWookiee: action.payload
  }),
  [SEARCH_RECEIVE_MORE_PLANETS_WOOKIEE]: (state, action) => ({
    planetsWookiee: _.concat(state.planetsWookiee, action.payload)
  }),
  [SEARCH_RECEIVE_SPECIES]: (state, action) => {
    const maxSpecies = _.maxBy(action.payload, s => s.average_height);
    return {
      species: action.payload,
      speciesMax: maxSpecies.average_height,
      speciesNext: action.next
    };
  },
  [SEARCH_RECEIVE_MORE_SPECIES]: (state, action) => {
    const species = _.concat(state.species, action.payload);
    const maxSpecies = _.maxBy(species, s => s.average_height);
    return {
      species,
      speciesMax: maxSpecies.average_height,
      speciesNext: action.next
    };
  },
  [SEARCH_RECEIVE_SPECIES_WOOKIEE]: (state, action) => ({
    speciesWookiee: action.payload
  }),
  [SEARCH_RECEIVE_MORE_SPECIES_WOOKIEE]: (state, action) => ({
    speciesWookiee: _.concat(state.speciesWookiee, action.payload)
  }),
  [SEARCH_RECEIVE_PEOPLE]: (state, action) => {
    const maxPerson = _.maxBy(action.payload, p => p.mass);
    return {
      people: action.payload,
      peopleMax: maxPerson.mass,
      peopleNext: action.next
    };
  },
  [SEARCH_RECEIVE_MORE_PEOPLE]: (state, action) => {
    const people = _.concat(state.people, action.payload);
    const maxPerson = _.maxBy(people, p => p.mass);
    return {
      people,
      peopleMax: maxPerson.mass,
      peopleNext: action.next
    };
  },
  [SEARCH_RECEIVE_PEOPLE_WOOKIEE]: (state, action) => ({
    peopleWookiee: action.payload
  }),
  [SEARCH_RECEIVE_MORE_PEOPLE_WOOKIEE]: (state, action) => ({
    peopleWookiee: _.concat(state.peopleWookiee, action.payload)
  }),
  [SEARCH_ACTIVATE_TAB]: (state, action) => ({
    activeTab: action.index
  }),
  [SEARCH_RESET]: state => ({
    ...initialState,
    wookiee: state.wookiee
  }),
  [SEARCH_TOGGLE_WOOKIEE]: state => ({
    wookiee: !state.wookiee
  })
};

export default createReducer(initialState, handlers);
