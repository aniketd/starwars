import _ from 'lodash';

export const createReducer = (initialState, handlers) => (
  (state = initialState, action) => {
    const handler = handlers[action.type];
    if (_.isUndefined(handler) === true) return state;
    return { ...state, ...handler(state, action) };
  }
);

export default { createReducer };
