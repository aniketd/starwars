import {
  SESSION_LOAD, SESSION_LOGIN, SESSION_LOGOUT
} from '../actions';
import { createReducer } from './utils';

export const initialState = {
  name: null,
  errors: []
};

const handlers = {
  [SESSION_LOAD]: (state, action) => ({
    name: action.payload,
    errors: action.errors
  }),
  [SESSION_LOGIN]: (state, action) => ({
    name: action.payload,
    errors: action.errors
  }),
  [SESSION_LOGOUT]: () => ({
    name: null,
    errors: []
  })
};

export default createReducer(initialState, handlers);
