import { combineReducers } from 'redux';

import session from './session';
import search from './search';

export default combineReducers({
  search,
  session
});
