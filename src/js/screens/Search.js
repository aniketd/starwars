import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';

import Header from 'grommet/components/Header';
import Box from 'grommet/components/Box';
import Title from 'grommet/components/Title';
import Anchor from 'grommet/components/Anchor';
import LogoutIcon from 'grommet/components/icons/base/Logout';
import Section from 'grommet/components/Section';
import CheckBox from 'grommet/components/CheckBox';
import { getMessage } from 'grommet/utils/Intl';

import SearchComponent from '../components/Search';
import { logout } from '../actions/session';
import { toggleWookiee } from '../actions/search';

const Search = ({ intl, wookiee, doToggleWookiee, doLogout }) => (
  <Box>
    <Header
      separator='bottom'
      full='horizontal'
      justify='between'
    >
      <Box>
        <Title>{getMessage(intl, 'StarWars')}</Title>
      </Box>
      <Box>
        <CheckBox
          checked={wookiee}
          id='wookiee'
          label='Wookiee?'
          name='wookiee'
          toggle={true}
          onChange={doToggleWookiee}
        />
      </Box>
      <Box>
        <Anchor
          icon={<LogoutIcon colorIndex='brand' />}
          label={getMessage(intl, 'Logout')}
          primary={true}
          reverse={true}
          onClick={doLogout}
        />
      </Box>
    </Header>
    <Section>
      <Box
        justify='center'
        full='horizontal'
        align='center'
      >
        <SearchComponent />
      </Box>
    </Section>
  </Box>
);

Search.propTypes = {
  doToggleWookiee: PropTypes.func.isRequired,
  doLogout: PropTypes.func.isRequired,
  wookiee: PropTypes.bool.isRequired,
  intl: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  wookiee: state.search.wookiee
});

const mapDispatchToProps = dispatch => ({
  doToggleWookiee: () => dispatch(toggleWookiee()),
  doLogout: () => dispatch(logout())
});

export default injectIntl(connect(
  mapStateToProps,
  mapDispatchToProps
)(Search));
