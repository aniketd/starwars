import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import Header from 'grommet/components/Header';
import Box from 'grommet/components/Box';
import Title from 'grommet/components/Title';
import Section from 'grommet/components/Section';
import { getMessage } from 'grommet/utils/Intl';

import LoginComponent from '../components/Login';

const Login = ({ intl }) => (
  <Box>
    <Header
      separator='bottom'
      full='horizontal'
      justify='between'
    >
      <Box>
        <Title>{getMessage(intl, 'StarWars')}</Title>
      </Box>
    </Header>
    <Section>
      <Box
        justify='center'
        full='horizontal'
        align='center'
      >
        <LoginComponent />
      </Box>
    </Section>
  </Box>
);

Login.propTypes = {
  intl: PropTypes.object.isRequired,
};

export default injectIntl(Login);
