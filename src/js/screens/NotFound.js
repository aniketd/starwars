import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';

import Box from 'grommet/components/Box';
import Headline from 'grommet/components/Headline';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import { getMessage } from 'grommet/utils/Intl';

const NotFound = ({ intl }) => (
  <Box full={true} align='center' justify='center'>
    <Headline strong={true}>404</Headline>
    <Heading>Oops!</Heading>
    <Paragraph size='large' align='center'>
      {getMessage(intl, 'NotFound')}
    </Paragraph>
  </Box>
);

NotFound.propTypes = {
  intl: PropTypes.object.isRequired
};

export default injectIntl(NotFound);
